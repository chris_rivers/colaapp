//
//  CCOLUtility.hpp
//  ColaLib
//
//  Created by Chris on 18/03/2016.
//  Copyright © 2016 Chris Rivers. All rights reserved.
//

#ifndef CCOLUtility_hpp
#define CCOLUtility_hpp

#include <stdio.h>
#include <AudioToolbox/AudioToolbox.h>
#include <AVFoundation/AVFoundation.h>

void checkError(OSStatus error, const char *operation);

#endif /* CCOLUtility_hpp */
