//
//  COLComponents.h
//  ColaLib
//
//  Created by Chris on 18/02/2015.
//  Copyright (c) 2015 Chris Rivers. All rights reserved.
//

#ifndef ColaLib_COLComponents_h
#define ColaLib_COLComponents_h

#import "COLComponentVCO.h"
#import "COLComponentLFO.h"
#import "COLComponentWavePlayer.h"
#import "COLComponentEnvelope.h"
#import "COLKeyboardComponent.h"
#import "COLComponentVCA.h"
#import "COLComponentMultiples.h"
#import "COLComponentMultiplesKB.h"
#import "COLComponentMixer2.h"
#import "COLComponentVCF.h"
#import "COLComponentPan.h"
#import "COLComponentRingModulator.h"
#import "COLComponentSequencer.h"
#import "COLComponentNoiseGenerator.h"
#import "COLComponentDelay.h"

#endif
