//
//  RotaryEncoder.h
//  ColaApp
//
//  Created by Chris on 23/03/2015.
//  Copyright (c) 2015 Chris Rivers. All rights reserved.
//
#import <ColaLib/CCOLTypes.h>
#import <UIKit/UIKit.h>
#import "ControlView.h"

@class ControlDescription;
@interface RotaryEncoder : ControlView

@property (nonatomic) double value;

@end
