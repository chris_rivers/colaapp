//
//  SequencerLED.h
//  ColaApp
//
//  Created by Chris on 26/01/2016.
//  Copyright © 2016 Chris Rivers. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface SequencerLED : UIView

@property (nonatomic) float level;
@property (nonatomic) bool blinking;

@end
