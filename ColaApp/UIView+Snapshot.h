//
//  UIView+Snapshot.h
//  ColaApp
//
//  Created by Chris on 14/04/2015.
//  Copyright (c) 2015 Chris Rivers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Snapshot)

-(UIImage*)snapshot;

@end
