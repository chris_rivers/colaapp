//
//  BuildViewCableLayer.h
//  ColaApp
//
//  Created by Chris on 24/03/2015.
//  Copyright (c) 2015 Chris Rivers. All rights reserved.
//

@class BuildView;
@interface BuildViewCableLayer : CALayer

@property (nonatomic, weak) BuildView *buildView;
@property CGFloat cableSway;


@end
