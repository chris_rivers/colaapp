//
//  BuildViewScrollView.h
//  ColaApp
//
//  Created by Chris on 22/04/2015.
//  Copyright (c) 2015 Chris Rivers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuildViewScrollView : UIScrollView

@property (nonatomic) BOOL enableAutoscroll;

@end
