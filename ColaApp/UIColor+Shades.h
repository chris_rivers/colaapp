//
//  UIColor+Shades.h
//  ColaApp
//
//  Created by Chris on 25/03/2015.
//  Copyright (c) 2015 Chris Rivers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Shades)

-(UIColor*)lighterShade;
-(UIColor*)midShade;
-(UIColor*)darkerShade;

@end
